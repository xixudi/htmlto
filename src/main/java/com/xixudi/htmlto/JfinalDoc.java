package com.xixudi.htmlto;


import com.jfinal.kit.StrKit;
import com.xixudi.htmlto.common.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @desc jfinal doc 爬取并生成chm
 * @author YangZheng 328170112@qq.com
 * @date 2019-02-19 9:40
 */
public class JfinalDoc {

    private static String url="https://www.jfinal.com/doc";
    private static String baseUrl="https://www.jfinal.com";
    private static String projectName="jfinal";
    private static String basePath= SomeKit.getPath(projectName);

    static class JfinalDownNode extends DownNode {
        JfinalDownNode(String url, String type, boolean cover) {
            super(url, type, cover);
        }

        JfinalDownNode(String url, String type) {
            super(url, type,false);
        }

        @Override
        protected String makeUrl() {
            if(url.startsWith("/")) {
                if(url.startsWith("//")) url = "http:"+url;
                else url = baseUrl+url;
            }
            return url;
        }

        @Override
        protected String makeNewFilePath() {
            return basePath + "/source/" + ("html".equals(type) ? "" : type + "/") + name;
        }
    }

    public static void downPage(String url) {
        try {
            JfinalDownNode page = new JfinalDownNode(url, "html",true);
            Document doc = page.getDoc();
            Elements csss = doc.select("link[href]");
            for (Element css : csss) {
                JfinalDownNode n = new JfinalDownNode(css.attr("href"), "css");
                n.saveToLocal();
                css.attr("href","./css/"+n.getName());
            }
            doc.select("link[href]:last-child").after("<link rel=\"stylesheet\" type=\"text/css\" href=\"./css/fix.css\">");
            Elements jss = doc.select("script[src]");
            for (Element js : jss) {
                JfinalDownNode n = new JfinalDownNode(js.attr("src"), "js");
                if(n.getName().startsWith("jquery")) {
                    js.attr("src","./js/jquery-1.12.4.min.js");
                } else if(n.getName().startsWith("bootstrap")) {
                    js.remove();
                } else {
                    js.attr("src", "./js/" + n.getName());
                    n.saveToLocal();
                }
            }
//            doc.select("script").remove();
            Elements imgs = doc.select("img[src]");
            for (Element img : imgs) {
                JfinalDownNode n = new JfinalDownNode(img.attr("src"), "img");
                n.saveToLocal();
                img.attr("src","./img/"+n.getName());
            }
            doc.select(".jf-header-box,.jf-footer-box,.doc-menu-box,meta:not([http-equiv=\"content-type\"]),style:not([href])").remove();
            doc.select(".jf-header,.jf-footer,.jf-doc-menus,meta:not([http-equiv=\"content-type\"]),style:not([href])").remove();
            Elements ps = doc.select(".jf-doc-content>p:last-child");
            if(ps.size()>0) {
                Element p = ps.get(0);
                if ("<br>".equals(p.html())) {
                    p.remove();
                }
            }
            Elements menus = doc.select(".doc-pre-next-box a[href~=/doc/*]");
            for (Element a : menus) {
                a.attr("href",a.attr("href").replaceAll("^/doc",".")+".html");
            }
            Element title = doc.select(".jf-doc-title").get(0);
            doc.title(title.text());
            doc.html(doc.html().replace("$(\"#docscroll\").scrollTop($currentMenu.offset().top - 110);",""));
            page.saveDocToLocal();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static ArrayList<ContentsNode> downResource(){
        ArrayList<ContentsNode> nodes = new ArrayList<>();
        try {
            Document doc = Jsoup.connect(url).get();
            Elements menus = doc.select(".jf-doc-menus a");
            for (Element menu : menus) {
                String href = menu.attr("href");
                if(href.startsWith("/doc")) {
                    nodes.add(ContentsNode.newTopic(menu.text(),href.substring(5)+".html"));
                    downPage(href);
                } else {
                    nodes.add(ContentsNode.newHeading(menu.text()));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nodes;
    }
    private static void appendPage(StringBuilder sb,List<ContentsNode> nodes,boolean pageBreak){
        for (ContentsNode node : nodes) {
            if(StrKit.notBlank(node.getPath())) {
                try {
                    Document doc = Jsoup.parse(new File(basePath + "/source/" + node.getPath()), "utf8");
                    Element content = doc.selectFirst(".jf-doc-contentbox");
                    content.select(".doc-pre-next-box").remove();
                    sb.append(content.html());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(pageBreak) sb.append("<h0>"+node.getTitle()+"</h0>");
            if(node.getChildren()!=null) {
                appendPage(sb,node.getChildren(),false);
            }
        }
    }
    private static String makePageStr(List<ContentsNode> nodes,boolean pageBreak){
        StringBuilder sb = new StringBuilder();
        appendPage(sb,nodes,pageBreak);
        return sb.toString();
    }
    public static void main(String[] args) throws IOException {
        ArrayList<ContentsNode> contentsDownNodes = downResource();
        String today = new SimpleDateFormat("yyyyMMdd").format(new Date());
        //生成chm
        MakeChm.createChm(projectName, contentsDownNodes, "index.html", "Jfinal帮助文档@"+today, true);
        //为生成pdf,拼接完整html
        String pageStr = makePageStr(ContentsNode.makeTreeData(contentsDownNodes), true);
        String html = SomeKit.renderTemplate(projectName,"temp.html",pageStr);
        //为pdf准备h1,h2的目录条目
        html = html.replaceAll("(</?h)2","$13")
                .replaceAll("(</?h)1","$12")
                .replaceAll("(</?h)0","$11");
        String allHtmlPath = basePath + "/source/all.html";
        Files.write(Paths.get(allHtmlPath),html.getBytes("utf8"));
        MakePDF.htmlToPdf(allHtmlPath, basePath+"/Jfinal帮助文档@"+today+".pdf");
    }
}